import { PageSizes, StandardFonts } from 'https://cdn.skypack.dev/pdf-lib@1.17.1?min'
import * as fflate from 'https://cdn.skypack.dev/fflate@0.8.0?min'
import mkEl from './mkel.js'

console.log(PageSizes)

function base64ToBytes(base64) {
  const binString = atob(base64);
  return Uint8Array.from(binString, (m) => m.codePointAt(0));
}

function bytesToBase64(bytes) {
  const binString = String.fromCodePoint(...bytes);
  return btoa(binString).replace(/=/g,'');
}

// This will allow to set all tmpl config to the url
// Limit it to 2000 chars http://stackoverflow.com/questions/417142/ddg#417184
const str = JSON.stringify(PageSizes)
const zip = fflate.zlibSync(fflate.strToU8(str), { level: 9, mem: 10 })
const enc = bytesToBase64(zip)
console.log('ZIP:', zip)
console.log('Encode:', enc.length, (100*enc.length/str.length).toFixed(2)+'%', enc)
console.log('Decoded:', fflate.strFromU8(fflate.unzlibSync(base64ToBytes(enc))))

const $ = (sel)=> mkEl.extend(document.querySelector(sel))

$('#tmpl-desc .help').title = `Label Elements Description:

<element-type>: <attr1>=<val1> <attr2>=<val2> <attr2>=<val2> ...
<element-type>: <attr1>=<val1> <attr2>=<val2> <attr2>=<val2> ...
<element-type>: <attr1>=<val1> <attr2>=<val2> <attr2>=<val2> ...

Element Types:
  • RECT
  • ELLIPSE
  • IMG
  • TEXT

Attributes:
  • x: number for horizontal positioning
  • y: number for vertical positioning
  • width: number
  • height: number
  • borderWidth: number
  • borderColor: [<red>, <green>, <blue>]
  • color: [<red>, <green>, <blue>]
  • font: string of a standard fonts name
  • maxWidth: number for TEXT box width
  • fontSize: number for font... size
  • lineHeight: number for TEXT line space
  • src: string for IMG source
  • content: string for TEXT
  • opacity: number
Find more attributes at https://pdf-lib.js.org/docs/api/classes/pdfpage

All position and size attributes are represented in milimeters.
`

const fields = {
  pageSizeSel: $('#page-size'),
  pageMarginT: $('#tmpl-margin__T'),
  pageMarginB: $('#tmpl-margin__B'),
  pageMarginL: $('#tmpl-margin__L'),
  pageMarginR: $('#tmpl-margin__R'),
  labelWidth:  $('#tmpl-label__W'),
  labelHeight: $('#tmpl-label__H'),
  labelGapH:   $('#tmpl-label__GH'),
  labelGapV:   $('#tmpl-label__GV'),
  tmplDesc:    $('#tmpl-desc textarea'),
  labelCSV:    $('#label-data textarea'),
}

for (let size in PageSizes) {
  let opt = fields.pageSizeSel.mkChild('option', { text: size })
  if (size === 'A4') opt.selected = true
}
