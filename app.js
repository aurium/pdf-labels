import './ui.js'

import {
  PDFDocument,
  PageSizes,
  StandardFonts,
  LineCapStyle,
  rgb,
  grayscale,
  degrees
} from 'https://cdn.skypack.dev/pdf-lib@1.17.1?min'

async function createPdf() {
  // Create a new PDFDocument
  const pdfDoc = await PDFDocument.create()

  // Add a blank page to the document
  const page = pdfDoc.addPage(PageSizes.A4)

  // Get the width and height of the page
  const { width, height } = page.getSize()

  const mm = height / 297 // Knowing it's an A4, find the relation pdfUnit/milimeter.
  const cm = 10*mm

  const printMargin = { top: 9*mm, bottom: 2*cm, left: 9*mm, right: 9*mm }
  const gap = { horiz: 5*mm, vert: 10*mm }

  const tagSize = { width: 9.35*cm, height: 4.5*cm }
  const cols = 2, rows = 5

  if (width < (printMargin.left+printMargin.right + (gap.horiz + tagSize.width)*(cols-1) + tagSize.width)) {
    alert('Ultrapassou a margem de impressão na largura.')
  }
  if (height < (printMargin.top+printMargin.bottom + (gap.vert + tagSize.height)*(rows-1) + tagSize.height)) {
    alert('Ultrapassou a margem de impressão na altura.')
  }

  console.log(StandardFonts)
  const fontHelvetica = await pdfDoc.embedFont(StandardFonts.Helvetica)

  const pngBytes = await fetch('pic.png').then(res => res.arrayBuffer())
  const pngImage = await pdfDoc.embedPng(pngBytes)
  console.log('Image:', pngImage)

  for (let row=0; row<rows; row++) for (let col=0; col<cols; col++) {
    let left = printMargin.left + (gap.horiz + tagSize.width)*col
    let top = height - ( printMargin.top + (gap.vert + tagSize.height)*row )
    drawLabel(left, top)
  }

  function drawLabel(left, top) {
    page.drawImage(pngImage, {
      x: left + 5*mm,
      y: top - 5*mm - 31,
      width: 32,
      height: 31,
    })

    page.drawRectangle({
      x: left,
      y: top - tagSize.height,
      width: tagSize.width,
      height: tagSize.height,
      //rotate: degrees(-15),
      borderWidth: 1,
      borderDashArray: [0,3,5,3],
      borderLineCap: LineCapStyle.Round,
      borderColor: grayscale(0.5),
      //color: rgb(0, .5, 1),
      borderOpacity: 0.5,
    })
    page.drawText(
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor, '+
      'leo a pharetra pretium, tortor felis viverra urna, a tincidunt turpis erat sit amet lacus. '+
      ( Math.random() < .5 ?
      'Suspendisse in nulla lorem. Aliquam pretium lectus sit amet velit interdum mollis.' : ''),
      {
        x: left + 1*cm,
        y: top - 1*cm,
        maxWidth: tagSize.width - 2*cm,
        size: 12,
        lineHeight: 14,
        font: fontHelvetica,
        color: grayscale(0),
      }
    )
  }

  // Serialize the PDFDocument to bytes (a Uint8Array)
  const pdfBytes = await pdfDoc.save()
  const url = URL.createObjectURL(new Blob([pdfBytes], { type: 'application/pdf' }))
  console.log(url)
  document.getElementById('pdf-viewer').src = url
  URL.revokeObjectURL(url)
}

createPdf()
